In order to use this system, the following commands will need to be changed:
- Nodejs/mongo.js
    Update "VMIP" to your Systems IP Address [Line 22]

Running the System:
- To run the system, you must first;
- enter the directory 
    "cd 6130comp"
- run the Docker Compose command
    "sudo docker-compose up"

Features:
- 3 Tiers (Web Service, Messaging Service [RabbitMQ] and MongoDB) are created from the use of the Docker File
- Restful API will recieve GET and POST requests
- If a Web Service Container goes down, a new node will be created.
- A Web Service Container will be made the parent node, and will carry out the commands
- If this container goes down, a new node is elected the leader
- If between the start and end time of the pre-planned scaler, new nodes will be made. Once the time has passed, the node will be killed.
    Update "loadStartTime" to Select Start Time [Line 19]
    Update "loadEndTime" to Select End Time [Line 20]

Tests:
- Create Enviroment (by running Docker-Compose [See "Running the System" for details] )
- Delete non-parent node to show it create new node
- Delete parent node to show it both elects new node and creates a new node
- Change Time to show that system will scale up (create new node) when between Start and End of Scheduled Scaling Time
- Change Time to show that system will scale down (destroy new node) when outside of Start and End of Scheduled Scaling Time


