const mongoose = require('mongoose');

//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

systemLeader = 0

var MaxWebNodes = 3 //changing this will increase and decrease amount of nodes created (max = 5)
var loadStartTime = 17 //change to set time additional node is created
var loadEndTime = 19 //change to set time additional node is stopped
var amountOfNodes = 1 //Amount of nodes to be created for scaling
var vmip = "192.168.56.40" //Change to IP of VM

var os = require("os");
var myhostname = os.hostname();
var imagename = "6130comp_node1"
var tempcount = 0
var portcount = 84;
var request = require('request');

//Class for Data Structure for Node Data
class node{
  constructor(name, nodeid){
    this.name = name;
    this.nodeid = nodeid;
  }
}

//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/notflixDB?replicaSet=cfgrs';

setInterval(function() {

  console.log(`Intervals are used to fire a function for the lifetime of an application.`);

}, 3000);

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var dataSchema = new Schema({
  AccountID: Number,
  UserName: String,
  TitleID: Number,
  UserAction: String,
  DateTime: Date,
  PointInteraction: Number,
  TypeInteraction: String
});

var dataModel = mongoose.model('Data',dataSchema, 'data');

app.get('/', (req, res) => {
  dataModel.find({},'AccountID UserName TitleID UserAction DateTime PointInteraction TypeInteraction', (err, data) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(data))
  })
})

app.post('/',  (req, res) => {
  var new_instance = new dataModel(req.body);
  new_instance.save(function (err) {
  if (err) res.send('Error');
  res.send(JSON.stringify(req.body))
});
})

//bind the express web service to the port specified
app.listen(port, () => {
console.log(`Express Application listening at port ` + port)
})

//This is the URL endopint of your vm running docker
var url = "http://"+vmip+":2375";

var amqp = require('amqplib/callback_api');

amqp.connect("amqp://user:bitnami@"+vmip, function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {});
});

var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);
var toSend = { "name" : myhostname, message:{"nodeid" : nodeID }} ;

setInterval(function() {
amqp.connect("amqp://user:bitnami@"+vmip, function(error0, connection) {
if (error0) {
        throw error0;
      }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              var msg =  JSON.stringify(toSend);

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });
              channel.publish(exchange, '', Buffer.from(msg));
              console.log(" [x] Sent %s", msg);
            });
            
            setTimeout(function(){
              connection.close();
            }, 500);
});
}, 1000);
var nodes = [];
amqp.connect("amqp://user:bitnami@"+vmip, function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });

              channel.assertQueue('', {
                        exclusive: true
                      }, function(error2, q) {
                                if (error2) {
                                            throw error2;
                                          }
                                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                                channel.bindQueue(q.queue, exchange, '');

                                channel.consume(q.queue, function(msg) {
                                  var nodestring = JSON.parse(msg.content);
                                            if(msg.content) {
                                                            console.log(" [x] %s", nodestring.message.nodeid);
                                                            const current = new node(nodestring.name, nodestring.message.nodeid)
                                                            var flag = false;
                                                            if(nodes.length === 0){
                                                              nodes.push(current);
                                                              console.log("Node Added - Array = 0");
                                                             }
                                                             else{
                                                             nodes.forEach(element => {
                                                             if(element.name == current.name){
                                                                 flag = true;
                                                               }
                                                              });
                                                             if(!flag){
                                                                nodes.push(current);
                                                                console.log("Node Added - New Node");
                                                              }
                                                            }
                                                            }
                                          }, {
                                                      noAck: true
                                                    });
                              });
            });
          });

setInterval(function() {
  console.log("Array Length: "+ nodes.length)
  leader = 1;
  activeNodes = 0;
  nodes.forEach(obj => {
     if(obj.name != myhostname){
        activeNodes++;
        if(obj.nodeid > nodeID)
        {
          leader = 0;
        }
    }
    if((leader == 1) && (activeNodes == (nodes.length - 1)))
    {
      systemLeader = 1;
    }
  });
}, 3000);

setInterval(function() {
nodes = [];
}, 10000);

setInterval(function() {
 if (systemLeader == 1){
console.log("Leader Elected: " + myhostname);

function containerCreate(imagename, port){
  portStr = port.toString();
  var create = {
    uri: url + "/v1.41/containers/create",
  method: 'POST',
    //deploy an alpine container that runs echo hello world
  json: {"Hostname":"node4","Image":imagename,"ExposedPorts":{"3000/tcp":{}},"HostConfig": {"PortBindings": {"3000/tcp": [{"HostPort": portStr}]}}}}

//send the create request
request(create, function (error, response, createBody) {
  if (!error) {
    console.log("Created container " + JSON.stringify(createBody));
   
      //post object for the container start request
      var start = {
          uri: url + "/v1.40/containers/" + createBody.Id + "/start",
        method: 'POST',
        json: {}
    };
  
    //send the start request
      request(start, function (error, response, startBody) {
        if (!error) {
          console.log("Container start completed");
    
              //post object for  wait 
              var wait = {
            uri: url + "/v1.40/containers/" + createBody.Id + "/wait",
                  method: 'POST',
              json: {}
          };
     
              
        request(wait, function (error, response, waitBody ) {
            if (!error) {
              console.log("run wait complete, container will have started");
                
                      //send a simple get request for stdout from the container
                      request.get({
                          url: url + "/v1.40/containers/" + createBody.Id + "/logs?stdout=1",
                          }, (err, res, data) => {
                                  if (err) {
                                      console.log('Error:', err);
                                  } else if (res.statusCode !== 200) {
                                      console.log('Status:', res.statusCode);
                                  } else{
                                      //we need to parse the json response to access
                                      console.log("Container stdout = " + data);
                                      return data.Id
                                  }
                              });
                      }
          });
          }
      });

  }   
});
}
var count = 0;
function containerQty(){
  request.get({
    //we are using the /info url to get the base docker information
      url: url + "/info",
  }, (err, res, data) => {
      if (err) {
          console.log('Error:', err);
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
      } else{
        //we need to parse the json response to access
          data = JSON.parse(data)
          console.log("Number of Containers = " + data.Containers);
          console.log("Containers running =" + data.ContainersRunning)
         
          if(data.ContainersRunning < (MaxWebNodes+8)){
            request.get({
            url: url + "/v1.40/containers/json?all=1&before=8dfafdbc3a40&size=1",
          }, (err, res, data) => {
            var arr = [];
            
            if (err) {
                console.log('Error:', err)
            } else if (res.statusCode !== 200) {
              console.log('Status:', res.statusCode)
            } else{
              arr = JSON.parse(data);
             arr.forEach(element => {
              if(JSON.stringify(element.Image).includes(imagename) && JSON.stringify(element.State).includes("running")){
                count++
                console.log("Count "+ count);
              }
             });
             if (count != 0){
              while(count < MaxWebNodes){
              containerCreate(imagename, portcount);
              portcount++
              count++
            }}
            }
          })
          }        
      }
  })
}
containerQty()
var tempNodeID;
function loadScaleTime(){
var sysDateTime = new Date(Date.now());
var currentTime = sysDateTime.getHours()+1
console.log("Current: " + currentTime + " - Start: "+ loadStartTime+ " - End: " + loadEndTime)
if ((currentTime > loadStartTime) && (currentTime < loadEndTime)){
  console.log("High Traffic Time Period: Create Node - " + tempcount + " / " + amountOfNodes)
  while (tempcount < amountOfNodes){
    tempcount++
    tempNodeID = containerCreate(imagename, portcount)
    portcount++
  }
}
else {
  var kill = {
    uri : url + "/v1.41/containers/" + tempNodeID +"/kill",
    method: "POST",
    json: {}
  }
  request(kill, function (error, response) {
    if (error){
      console.log('Error: ',err)
    } else if (response.statusCode !== 200) {
      console.log("Status: ", response.statusCode)
    }
    else {
    console.log("High Traffic Timer Period Ended: Stopped Node")
    }
  })
}
}
loadScaleTime()
}
else{
  console.log("I am NOT the leader")
}
}, 3000);
